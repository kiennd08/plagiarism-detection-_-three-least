package util;

import java.io.File;
import java.io.IOException;

import vn.hus.nlp.sd.SentenceDetector;
import vn.hus.nlp.sd.SentenceDetectorFactory;
import vn.hus.nlp.tokenizer.VietTokenizer;

public class DocumentUtil {

	private static VietTokenizer tokenizer;
	private static SentenceDetector sDetector;

	public DocumentUtil() {
		if (sDetector == null) {
			sDetector = SentenceDetectorFactory.create("vietnamese");
			tokenizer = new VietTokenizer();
		}
	}

	public String sentencesSpliter(File file) {

		String sentences[];
		try {
			sentences = sDetector.detectSentences(file.getAbsolutePath());
			String s = "";
			for (int j = 0; j < sentences.length; j++) {
				s = s + sentences[j] + "\n";
			}
			return s;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// tach thanh tung tu
	public void wordDetect(File input, File output) {
		tokenizer.tokenize(input.getAbsolutePath(), output.getAbsolutePath());
	}

	public static String removeAllEndLine(String s) {
		return s.replace("\n", " ").replace("\r", "");
	}

	public static String removeSpecialCharWithoutDot(String s) {
		return s.replaceAll("[^a-zA-Záàạảãâấầậẩẫăắằặẳẵéèẹẻẽêếềệểễóòọỏõôốồộổỗơớờợởỡúùụủũưứừựửữíìịỉĩđýỳỵỷỹ.]", "");
	}

	public static String removeSpecialChar(String s) {
		return s.replaceAll(
				"[^a-zA-Záàạảãâấầậẩẫăắằặẳẵéèẹẻẽêếềệểễóòọỏõôốồộổỗơớờợởỡúùụủũưứừựửữíìịỉĩđýỳỵỷỹ]",
				"");
	}

	public static String removeSpecialChar_WithoutBreakline(String s) {
		return s.replaceAll("[^a-zA-Záàạảãâấầậẩẫăắằặẳẵéèẹẻẽêếềệểễóòọỏõôốồộổỗơớờợởỡúùụủũưứừựửữíìịỉĩđýỳỵỷỹ\\n]", "");
	}

	
	public static void main(String[] args) {
		System.out.println(removeSpecialCharWithoutDot("! á. ý ỹ @#$%^& á ý a*()[0-9]ac /\\\\c,?><~`'_+-=:;\"\\n abcba"));
	}
}
