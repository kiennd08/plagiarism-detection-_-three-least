package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Vector;

public class FileUtil {

	public FileUtil() {
		// TODO Auto-generated constructor stub
	}

	// ghi text ra file
	public static void writeTexttoFile(String content, File file) {
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// doc tung dong cua 1 vector
	public static Vector<String> FiletoVectorText(String filename) {
		Vector<String> vec = new Vector<String>();
		try {
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line = null;
			while ((line = br.readLine()) != null) {
				vec.add(line);
			}
		} catch (Exception e) {
		}
		return vec;
	}

	public static String readTextFile(String fileName){
		String s = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line = null;
			while ((line = br.readLine()) != null) {
				s+=line+"\n";
			}
		} catch (Exception e) {
		}
		return s;
	}
	
	// chuye
	// coppy fileA sang fileB
	public static void coppyFile(File fileA, File fileB) {
		try {
			if (!fileB.exists()) {
				fileB.createNewFile();
			}
			InputStream is = new FileInputStream(fileA);
			OutputStream os = new FileOutputStream((fileB));
			byte[] buffer = new byte[1024];
			int length;
			// copy the file content in bytes
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			is.close();
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void ObjectToFile(Object obj,String filePath){
		FileOutputStream fout;
		try {
			fout = new FileOutputStream(filePath);
			ObjectOutputStream oos = new ObjectOutputStream(fout);   
			oos.writeObject(obj);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Object fileToObject(String filePath){

		FileInputStream fin;
		try {
			fin = new FileInputStream(filePath);
			ObjectInputStream ois = new ObjectInputStream(fin);
			Object obj = ois.readObject();
			ois.close();
			return obj;
		} catch (IOException | ClassNotFoundException e) {
//			e.printStackTrace();
		}
		return null;
	}
	

    public static void delete(File file)
    	throws IOException{
 
    	if(file.isDirectory()){
 
    		//directory is empty, then delete it
    		if(file.list().length==0){
 
    		   file.delete();
    		   System.out.println("Directory is deleted : " 
                                                 + file.getAbsolutePath());
 
    		}else{
 
    		   //list all the directory contents
        	   String files[] = file.list();
 
        	   for (String temp : files) {
        	      //construct the file structure
        	      File fileDelete = new File(file, temp);
 
        	      //recursive delete
        	     delete(fileDelete);
        	   }
 
        	   //check the directory again, if empty then delete it
        	   if(file.list().length==0){
           	     file.delete();
        	     System.out.println("Directory is deleted : " 
                                                  + file.getAbsolutePath());
        	   }
    		}
 
    	}else{
    		//if file, then delete it
    		file.delete();
    		System.out.println("File is deleted : " + file.getAbsolutePath());
    	}
    }

}
