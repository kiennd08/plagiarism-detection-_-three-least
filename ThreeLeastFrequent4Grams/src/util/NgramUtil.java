package util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import dao.NgramDataLoader;

public class NgramUtil {

	public static List<String> get3NGram(String sentence){
		List<String> result = new ArrayList<>();
		List<String> allGrams = new ArrayList<>();
		for (int i = 0; i < sentence.length() - 4; i++) {
			String gram = sentence.substring(i, i + 4);
			allGrams.add(gram);
		}
		if(allGrams.size()<3){
			return new ArrayList<>();
		}

		Collections.sort(allGrams,new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				// TODO Auto-generated method stub
				return NgramDataLoader.getInstance().getCountOfNgram(o1) - NgramDataLoader.getInstance().getCountOfNgram(o2);
			}
			
		});
		result = allGrams.subList(0, 3);
		return result;
	}
	
	public static List<String> textToThreeLeastList(File documentTxtFile){
		String [] sentences = new DocumentUtil().sentencesSpliter(documentTxtFile).split("\n");
		
		List<String> res = new ArrayList<>();
		for (String string : sentences) {
			string = DocumentUtil.removeSpecialChar(string);
			List<String> threeNgram = NgramUtil.get3NGram(string);
			if(threeNgram.size()>0){
				res.add(listToString(threeNgram));
			}
		}
		return res;
	}
	
	public static List<String> pdfToThreeLeastList(File pdfFile){
		String content = PdfReaderUtil.pdfToString(pdfFile);
		content = DocumentUtil.removeAllEndLine(content);
		File tmp = new File("tmp.txt");
		FileUtil.writeTexttoFile(content,tmp);
		return textToThreeLeastList(tmp);
		
	}
	
	
	private static String listToString(List<String> s){
		String res = "";
		for (String string : s) {
			res+= string;
		}
		return res;
	}

	
	
}
