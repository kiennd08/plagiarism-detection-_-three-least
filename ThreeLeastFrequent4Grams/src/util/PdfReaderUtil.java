package util;

import java.io.File;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

public class PdfReaderUtil {
	public static String pdfToString(File pdfFile) {
		try {
			PdfReader reader = new PdfReader(pdfFile.getAbsolutePath());
			int numPages = reader.getNumberOfPages();
			String s = "";
			System.out.println(numPages);
			for (int j = 1; j <= numPages; j++) {
				String page = PdfTextExtractor.getTextFromPage(reader, j);
				s = s + page;
			}
			return s;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	
}
