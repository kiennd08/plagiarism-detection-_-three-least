package dao;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import util.DocumentUtil;
import util.FileUtil;
import util.NgramUtil;
import util.PdfReaderUtil;

public class NgramDataLoader implements Serializable{
	/**
	 * 
	 */
	public static NgramDataLoader instance;
	
	
	
	private static final long serialVersionUID = 2590758859125582833L;
	private static String unicodeChar = "áàạảãâấầậẩẫăắằặẳẵéèẹẻẽêếềệểễóòọỏõôốồộổỗơớờợởỡúùụủũưứừựửữíìịỉĩđýỳỵỷỹ";
	private int ngrams[];
	
	
	private NgramDataLoader() {
		ngrams = new int[99999999];
	}
	
	public static NgramDataLoader getInstance(){
		if(instance == null){
			instance = loadSavedData();
		}
		return instance;
	}
	
	public void addNewFilePDF(File filePDF) {
		File parentFile = filePDF.getParentFile();
		File majorFolder = new File("data_training" + File.separator
				+ parentFile.getName());
		System.out.println(filePDF.getName());
		// make major folder
		if (!majorFolder.exists()) {
			majorFolder.mkdir();
		}

		String folderDocument = "data_training" + File.separator
				+ parentFile.getName() + File.separator + filePDF.getName();
		System.out.println(folderDocument);
		File file = new File(folderDocument);
		if (!file.exists()) {
			file.mkdir();
		} else {
			// if exist folder -> abort
			return;
		}
		String pathfileRoot = file.getAbsolutePath() + File.separator
				+ "content.txt";
		File fileRoot = new File(pathfileRoot);
		String s = PdfReaderUtil.pdfToString(filePDF);
		s = DocumentUtil.removeAllEndLine(s);
		FileUtil.writeTexttoFile(s, fileRoot);
		s = DocumentUtil.removeSpecialChar(s);
		
		// System.out.println(s);
		for (int i = 0; i < s.length() - 4; i++) {
			String gram = s.substring(i, i + 4);
			// System.out.println(gram);
			NgramDataLoader.getInstance().addGram(gram);
		}


	}

	
	public void resetData() throws IOException{
		instance = new NgramDataLoader();
		File data_training = new File("data_training");
		if(data_training.exists()){
			FileUtil.delete(data_training);
		}
		data_training.mkdir();
		File data = new File("data");
		File majorFiles[] = data.listFiles();
		for (int i = 0; i < majorFiles.length; i++) {
			File filePDFs[] = majorFiles[i].listFiles();
			for (int j = 0; j < filePDFs.length; j++) {
				addNewFilePDF(filePDFs[j]);
//				System.out.println();
			}
		}
		for (int i = 0; i < majorFiles.length; i++) {
			File filePDFs[] = majorFiles[i].listFiles();
			for (int j = 0; j < filePDFs.length; j++) {
				String path = "data_training" + File.separator
						+ filePDFs[j].getParentFile().getName() + File.separator + filePDFs[j].getName()+File.separator+"content.txt";
				GenerateSentenceNgramData(new File(path));
			}
		}
		
		
		saveData();
	}
	
	public void GenerateSentenceNgramData(File documentTxtFile) {
		String folderPath = documentTxtFile.getParent();
		String threeLeastPath = folderPath+ File.separator + "threeleast.txt";
		
		
		List<String> threeList = NgramUtil.textToThreeLeastList(documentTxtFile);
		String res = "";
		for (String string : threeList) {
			res+=string+"\n";
		}
		FileUtil.writeTexttoFile(res, new File(threeLeastPath));
	}
	
	
	
	
	private static NgramDataLoader loadSavedData(){
		NgramDataLoader data = (NgramDataLoader) FileUtil.fileToObject("ngramdata.data");
		if(data == null){
			data = new NgramDataLoader();
		}
		return data;
	}
	
	public void saveData(){
		FileUtil.ObjectToFile(instance, "ngramdata.data");
	}
	
	public void addGram(String ngram){
		ngram = ngram.toLowerCase();
		int index = gramToIndex(ngram);
		if(index == -1){
			return;
		}
		ngrams[index]++;
	}
	
	public int gramToIndex(String ngram){
		ngram = ngram.toLowerCase();
		int index = 0;
		for (int i = 0; i < ngram.length(); i++) {
			index *=100;
			int n = charToNum(ngram.charAt(i));
			if(n!=-1){				
				index+= n;
			}else{
				return -1;
			}
			
		}
		return index;
	}
	
	public int charToNum(char a){
		if(a>='a'&&a<='z'){
			return a-'a';
		}
		int i = unicodeChar.indexOf(a);
		if(i==-1){
			return -1;
		}else{
			return i+26;
		}
	}
	
	public int getCountOfNgram(String ngram){
		ngram = ngram.toLowerCase();
		int index = gramToIndex(ngram);
		if(index ==-1){
			return -1;
		}
		return ngrams[index];
	}
	
	public static void main(String[] args) {
//		String s = "bcbacb 3 @# 2#@3 232323@# !@#$%^&*()/\\,.?~'`_+-=><:;/\" \nchứa có";
//		System.out.println(s.replaceAll("[!@#$%^&*()[0-9] /\\\\,.?><~`'_+-=:;\"\\n]", ""));
//		
	//"áàạảãâấầậẩẫăắằặẳẵéèẹẻẽêếềệểễóòọỏõôốồộổỗơớờợởỡúùụủũưứừựửữíìịỉĩđýỳỵỷỹ"
//		System.out.println(new NgramData().gramToIndex("cchỹ"));
		System.out.println("abbcbcbhbahcbahcbh ahbchac hbachbahcb ".split("abbcbcbhbahcbahcbh").length);
	}
	
}
