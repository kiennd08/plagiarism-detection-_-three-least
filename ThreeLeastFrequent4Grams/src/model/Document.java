package model;

import java.io.Serializable;

public class Document implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Document() {
		// TODO Auto-generated constructor stub
	}

	private int idDoc;
	private String content;
	private String major;
	private String nameFile;

	public Document(int idDoc, String content, String major, String nameFile) {
		super();
		this.idDoc = idDoc;
		this.content = content;
		this.major = major;
		this.nameFile = nameFile;
	}

	public int getIdDoc() {
		return idDoc;
	}

	public void setIdDoc(int idDoc) {
		this.idDoc = idDoc;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

}
