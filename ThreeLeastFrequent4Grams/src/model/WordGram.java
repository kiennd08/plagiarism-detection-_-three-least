package model;

import java.io.Serializable;

public class WordGram implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	private int weigh;

	public WordGram() {
		// TODO Auto-generated constructor stub
	}

	public WordGram(String name) {
		this.name = name;
		this.weigh = 1;
	}

	public WordGram(String name, int weigh) {
		super();
		this.name = name;
		this.weigh = weigh;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeigh() {
		return weigh;
	}

	public void setWeigh(int weigh) {
		this.weigh = weigh;
	}

}
