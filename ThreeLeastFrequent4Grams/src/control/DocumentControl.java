package control;

import java.io.File;
import java.io.IOException;
import java.util.List;

import util.FileUtil;
import util.NgramUtil;
import dao.NgramDataLoader;

public class DocumentControl {

	public DocumentControl() {
		// TODO Auto-generated constructor stub
		
	}



	

	public static void main(String[] args) throws IOException {
		// new DocumentControl().learnAllData();
		//NgramDataLoader.getInstance().resetData();
//		System.out.println());
		List<String> threeLeastList =  NgramUtil.pdfToThreeLeastList(new File("a.pdf"));
		
		File f = new File("data_training/CNTT");
		String [] flist = f.list();
		int [] result = new int[flist.length+1];
		int i=0;
		
		
		for (String string2 : flist) {
			i++;
			String filePath2 = f.getAbsolutePath()+File.separator+string2 + File.separator+"threeleast.txt";
			String threeLeastF2 = FileUtil.readTextFile(filePath2);
		//	System.out.println(threeLeastF2);
		
			int count = 0;
			for (String string : threeLeastList) {
			//	System.out.println(string);
				String [] threeGrams = new  String[4];
				threeGrams[1] = string.substring(0,4);
				threeGrams[2] = string.substring(4,8);
				threeGrams[3] = string.substring(8,12);
				
				String s1 = threeGrams[1]+threeGrams[2]+threeGrams[3];
				String s2 = threeGrams[1]+threeGrams[3]+threeGrams[2];
				String s3 = threeGrams[2]+threeGrams[1]+threeGrams[3];
				String s4 = threeGrams[2]+threeGrams[3]+threeGrams[1];
				String s5 = threeGrams[3]+threeGrams[2]+threeGrams[1];
				String s6 = threeGrams[3]+threeGrams[1]+threeGrams[2];
				
				count += threeLeastF2.split(s1).length-1;
				
				count += threeLeastF2.split(s2).length-1;
				
				count += threeLeastF2.split(s3).length-1;
				
				count += threeLeastF2.split(s4).length-1;
				
				count += threeLeastF2.split(s5).length-1;
				count += threeLeastF2.split(s6).length-1;
				
			}
			System.out.println(count);
			
			
		}
		
		
		
		
		
	}

}
