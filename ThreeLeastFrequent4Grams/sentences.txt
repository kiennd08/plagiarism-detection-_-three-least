phân_loại đã đ ƣ ợc định_nghĩa .
 Bộ phân_loại BA phân_loại gói dựa trên_cơ sở_chỉ duy_nhất những điểm cốt_yếu của
một tr ƣ ờng tiêu_đề của dịch_vụ khác_biệt .
 Bộ phân_loại đa tr ƣ ờng MF ( multi - field ) thì ng ƣ ợc lại nó phân_loại dựa trên một
hay nhiều hơn một tr ƣ ờng tiêu_đề , nh ƣ địa_chỉ nguồn , địa_chỉ đích , tr ƣ ờng DS ,
giao_thức ID , số l ƣ ợng cổng nguồn … và các thông_tin khác nh ƣ giao_diện đến .
Dụng_cụ
đo
Luồng
Bộ lập Bộ đánh Định_hướng /
dữ_liệu
lịch dấu Loại_bỏ
Hình 4.22 : Điều_khiển lưu_lượng ở một node dịch_vụ khác_biệt
Những bộ phân_loại phân_loại gói đ ƣ ợc sử_dụng cho điều_khiển lộ_trình gói phù_hợp
một_số quy_tắc cho sự xử_lý sau_này .
Điều_khiển l ƣ u l ƣ ợng thực_hiện sự ghi lại , tạo
khuôn_dạng , kiểm_soát và đánh_dấu lại cho sự chắc_chắn rằng l ƣ u l ƣ ợng đăng_nhập miền
DS thực_hiện cho một_số điều_chỉnh đặc_biệt về l ƣ u l ƣ ợng trong TCS xem Hình 4.22 .
Trên cơ_sở điều_kiện l ƣ u l ƣ ợng đ ƣ ợc phân_loại cũng nh ƣ bên trong “ in - profile ” hay ngoài
“ out – profile ” .
Những gói bên ngoài của profile có_thể đựợc xếp_hàng cho_đến khi vào
Phó_Tiến_Dũng - D2001VT 105
Đồ_án tốt_nghiệp Kết_luận
trong hồ_sơ in - profile ( định h ƣ ớng ) , loại_bỏ ( kiểm_soát ) , đánh_dấu lại với code - point ( re -
maked ) , hoặc chuyển_đổi .
Xa hơn thế , các gói out - of - profile có_thể đ ƣ ợc lộ_trình hoá
một hay nhiều hơn một tổ_hợp tổ_hợp cách_thức .
Quản_lý bộ đệm và lập lịch của dịch_vụ khác_biệt
Không giống nh ƣ dịch_vụ khác_biệt về cách tiếp_cận khung dịch tích_hợp vì cách
tiếp_cận của dịch_vụ khác_biệt không xác_định rõ cấu_trúc lập lịch nh ƣ ng chỉ yêu_cầu
t ƣ ơng đối sự phân_loại l ƣ u l ƣ ợng khác_nhau .
Sự_thực thi đ ƣ ợc hình_thành đã loại_bỏ sự
sản xuốt phần_cứng .
Đến nay vẫn có kiến_trúc hàng đợi có tên là hàng đợi cơ_sở phân
loại ( CBQ : Class_Base_Queueing ) , nó d ƣ ờng nh ƣ là ứng_cử_viên sáng_giá cho lập lịch
khác_biệt phổ_biến .
CBG có_thể đ ƣ ợc cân_nhắc theo thứ_bậc , ƣ u tiên trọng số trên bộ
lập lịch WFQ ( xem Hình 4.19 ) .
Sự vận_hành dựa trên sự tập_hợp phân_loại l ƣ u l ƣ ợng .
Với CBQ , phân_cấp phân_loại l ƣ u l ƣ ợng đa mức một là có_thể đ ƣ ợc cấu_trúc ƣ u tiên
hay cấu_trúc trọng số , cơ_sở ƣ u thế của phân_loại này đ ƣ ợc thiết_lập .
Mỗi loại của
nhóm phân_loại có_thể tối_đa về băng_thông truy_nhập hay cho_phép m ƣ ợn từ nhóm
khác .
Hình 4.23 trình_diễn ví_dụ cho những l ƣ u l ƣ ợng Video , audio và hỗi trợ tối_đa
giống nh ƣ FTP hay telnet .
Hàng đợi cơ_sở phân_loại ( CBQ )
Trong miền dịch_vụ đ ƣ a ra có_thể đ ƣ ợc nhóm lại cho việc định l ƣ ợng hay đặc_tính
chất l ƣ ợng .
Sự phân_tích định l ƣ ợng dịch_vụ đ ƣ a ra đảm_bảo là khó_khăn nó có thể_xác
nhận bởi th ƣ ớc đo phù_hợp không t ƣ ơng thích bất_kì những dịch_vụ tồn_tại song_song
khác .
Định_tính chất l ƣ ợng dịch_vụ trên ph ƣ ơng thức khác thì không cung_cấp sự đảm
bảo chính_xác nh ƣ ng hơn thế t ƣ ơng quan với quyền ƣ u tiên phụ_thuộc vào những dịch
vụ rất nhiều .
Ở đây , sự xác_nhận các dịch_vụ chỉ có_thể đ ƣ ợc thực_hiện t ƣ ơng đối giữa
nhiều thành_phần dịch_vụ .
Một_cách khác của dịch_vụ khác_biệt đ ƣ ợc thể_hiện qua
ph ƣ ơng thức lựa_chọn dựa trên QoS đ ƣ ợc cung_cấp .
Những tiêu_chuẩn của dịch_vụ
Có những tiêu_chuẩn về dịch_vụ đ ƣ ợc IETF đ ƣ a ra .
( i ) Mặc_định ( best-effort ) PHB
( ii ) Nhóm bộ lựa_chọn phân_loại cho IP t ƣ ơng thích với quyền ƣ u tiên .
( iii ) Nhóm PHB chuyển_đổi giả_định
( iv ) tiến_hành chuyển_đổi PHB
Sự mặc_định PBH ( i ) phải có hiệu_lực trong tất_cả các node thụôc dịch_vụ khác_biệt .
Cho ví_dụ nh ƣ code-point „ 000000 ‟ cho toàn_bộ cách_thức mặc_định .
Ngoài_ra , không
một gói nào DSCPs không biết có_thể đ ƣ ợc vạch ra cho PHB mặc_định .
Nhóm bộ
phân_loại PHB
( ii ) thiết_lập khả_năng backwark cho những khái định_nghĩa tr ƣ ờng ToS .
Cách viết
khác , phụ_thuộc vào code – point với giá_trị „ xxx000 ‟ có_thể đ ƣ ợc l ƣ ợc đồ hoá cho
những PHB nó t ƣ ơng hợp tốt nhất với mã quyền ƣ u tiên IP cá_biệt .
Tám mã code -
points thiết_lập mô_hình khác_biệt dịch_vụ t ƣ ơng ứng .
( iii ) nhóm PHB chuyển_đổi giả_định có hai tham_số ( x ) đ ƣ ợc dùng để xác_định thứ
tự_quyền ƣ u tiên .
( y ) đ ƣ ợc sử_dụng để định_nghĩa cho sự xác_định cấp_bậc mức_độ
quan_trọng để loại_bỏ gói .
Ví_dụ : cho AF11 và AF12 vào hàng xếp cho bộ đệm giống
nhau tuy_nhiên AF12 có khả_năng bị loại_bỏ khỏi bộ đệm cao hơn trong tr ƣ ờng hợp
xảy_ra tắc_nghẽn .
Chú_ý rằng có_thể thấy rõ rằng ta có_thể sử_dụng bộ đệm khác_nhau
cho AF11 và AF12 , tuy_nhiên phải tránh sự sắp_xếp lại gói .
Giá_trị 12AF code - points
đ ƣ ợc phân_chia vào trong 4 phân_loại AF và 3 thứ_tự mức ƣ u tiên loại_bỏ trong mỗi
phân_loại AF .
Phó_Tiến_Dũng - D2001VT 107
Đồ_án tốt_nghiệp Kết_luận
( iv ) cung_cấp dịch_vụ định l ƣ ợng với sự đảm_bảo hà_khắc , đó là mất gói thấp , sự
tiềm_tàng phải nhỏ , biến_động trễ thấp và cấp cho tốc_độ đỉnh tuỳ theo bản thoả_thuận
quyết_định tốc_độ bit .
Dịch_vụ này xuốt hiện nh ƣ đ ƣ ờng dây thuê riêng ảo „ virtual
leased line ‟ cho khách_hàng .
Nếu quyền ƣ u tiên không giới_hạn của l ƣ u l ƣ ợng EF
đ ƣ ợc cho_phép trong hệ_thống sau khi sử_dụng tài_nguyên của tầng phân_loại EF phải
có giới_hạn cốt để tránh sự thiếu_thốn của l ƣ u l ƣ ợng ƣ u tiên bị giảm xuống .
Thu_hồi băng_thông
Những phần_tử thu_hồi băng_thông ( BB : Bandwidth_Brokering ) thực_hiện quản_lý
mềm_dẻo nguồn tài_nguyên trong mạng dịch_vụ khác_biệt ( ví_dụ : Call admission
control , cấu_hình của router , tín_hiệu phân_chia tài_nguyên , sự nhận_thực ) .
BBs không
chỉ chịu trách_nhiệm cho quản_lý tài_nguyên , chúng cũng điều_khiển sự liên_lạc miền
liên_kết cốt để đảm_bảo chất l ƣ ợng dịch_vụ cho l ƣ u l ƣ ợng đa miền .
Từ vẻ bề_ngoài
này , BBs có_thể đ ƣ ợc quan_tâm các tác_nhân nguồn tài_nguyên , chúng tiếp_nhận yêu
cầu ng ƣ ời sử_dụng và quyết_định có hay không chia_sẻ giá_trị nguồn tài_nguyên giữa
những ng ƣ ời sử_dụng cho những yêu_cầu của chúng .
Nếu câu trả_lời là có , chúng để
dành những yêu_cầu tổng_số của nguồn tài_nguyên cho những yêu_cầu sử_dụng bởi
cấu_hình những node mạng phụ_thuộc vào những dịch_vụ đã đ ƣ ợc xác_định rõ .
BBs
đ ƣ ợc đăng_ký cho những miền .
Kết_luận ch ƣ ơng
Ch ƣ ơng IV trình_bày cụ_thể từng kỹ_thuật trong mạng IP có tham_gia vào quá_trình
xử_lý luồng , các giải_pháp , cơ_chế hay nguyên_lý hoạt_động trong các kỹ_thuật .
Trong
quá_trình xử_lý một_số vấn_đề phát_sinh nh ƣ nghẽn mạng … đòi_hỏi phải có những
ph ƣ ơng thức xử_lý thích_hợp , Ở chuơng này đ ƣ a ra những giải_pháp đó .
Từ đó thấy đ ƣ ợc mối quan_hệ t ƣ ơng quan cụ_thể tới QoS trong mạng .
Cách_thức xử
lý , quản_lý để nâng cao chất l ƣ ợng dịch_vụ mạng .
Phó_Tiến_Dũng - D2001VT 108
Đồ_án tốt_nghiệp Kết_luận
KẾT_LUẬN
Sau một thời_gian tìm_hiểu bản_đồ án ” nghiên_cứu chất_lượng dịch_vụ trong mạng
IP” đã hoàn_thành với những nội dụng chính sau đây :
 Tìm_hiểu tổng_quan về mô_hình TCP/IP
 Tìm_hiểu chung về QoS .
 Nghiên_cứu tổng_quan về chất_lượng dịch_vụ trong mạng IP .
 Nghiên_cứu một_số kỹ_thuật hỗi trợ chất_lượng dịch_vụ trong mạng IP .
Bản_đồ án tìm_hiểu QoS theo một trình_tự : từ cái nhìn_chung về QoS đến một quan
điểm cụ_thể QoS trong mạng IP , trong đó nhấn_mạnh về khía_cạnh quản_lý mạng .
Do kiến_thức còn nhiều hạn_chế và thời_gian có_hạn , lĩnh_vực còn mới nên đồ_án
mới chỉ tìm_hiểu dưới dạng tổng_quan , không đi_sâu vào những nội_dung cụ_thể .
Bài
toán nâng cao chất_lượng dịch_vụ mới chỉ xét trên quan_điểm của nhà quản_lý mạng ,
còn một_số quan_điểm của người sử_dụng cụ_thể , quan_điểm nâng cao chất_lượng bằng
phương_pháp quản_lý nhân_lực … không được trình_bày ở đây .
Mô_hình mạng từ biên
tới biên mới chỉ đưa ra hai mô_hình intserv và diffserv còn những mô_hình khác chưa
được nêu ra ở đây .
Từ những kết_quả đạt được trong bản_đồ án này có_thể tiếp_tục đi_sâu nghiên_cứu
chi_tiết hơn về các giải_pháp , những giao_thức , và một_số mô_hình dịch_vụ mạng khác
hỗi trợ QoS trong mạng IP .
Mặc_dù đã hết_sức cố_gắng nhưng đồ_án không tránh khỏi có những thiếu_sót .
Em
xin được tiếp_tục tiếp_thu mọi ý_kiến góp_ý , sửa_chữa của các thầy_cô và các bạn để có
thể hoàn_thiện đề_tài này sâu hơn .
Để có được kết_quả như ngày hôm_nay là nhờ công_lao dạy_dỗ của các thầy_cô
giáo , đặc_biệt là TS .
Nguyễn_Tiến_Ban đã hướng_dẫn , giúp_đỡ em trong công_việc
cùng_với sự giúp_đỡ của gia_đình cùng và các bạn .
Em xin gửi lời cảm_ơn chân_thành
tới thày cô_giáo và gia_đình , bạn_bè đã sát_cánh với em , giúp_đỡ em .
Xin chân_thành cảm_ơn !
SV : Phó_Tiến_Dũng
Phó_Tiến_Dũng - D2001VT 109
